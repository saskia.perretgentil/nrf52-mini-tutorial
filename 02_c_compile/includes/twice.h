#ifndef TWICE_H
#define TWICE_H

int twice(int);
float twice_f(float);

#endif // TWICE_H
