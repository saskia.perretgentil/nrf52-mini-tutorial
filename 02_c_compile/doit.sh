#!/bin/sh

ASFLAGS="-g "
# ASFLAGS="-mcpu=cortex-m4 "
#ASFLAGS+="-mthumb "
#ASFLAGS+="-mfloat-abi=hard -mfpu=fpv4-sp-d16 "

CFLAGS="-g " # add debug information
CFLAGS+="-c " # compile only, no linky linky!
CFLAGS+="-mcpu=cortex-m4 "
CFLAGS+=" -mthumb -mabi=aapcs --std=gnu99 "
CFLAGS+="-Wall "
CFLAGS+="-O0 " # Dont optimize.
CFLAGS+="-mfloat-abi=hard -mfpu=fpv4-sp-d16 "

LDFLAGS="-mfloat-abi=hard -mfpu=fpv4-sp-d16 "
LDFLAGS+="-mcpu=cortex-m4 "
LDFLAGS+="-mthumb -mabi=aapcs --std=gnu99 "
LDFLAGS+="-Wl,--gc-sections "
LDFLAGS+="-Xlinker -Map=capp.map "
LDFLAGS+="-Tnrf52.ld "
LDFLAGS+="--specs=nano.specs "
LDFLAGS+="-lc -lnosys -lm"

# Compile the files
# -c flag tells gcc to only compile, without linking
# -g flag tells gcc to insert debug information into object file
arm-none-eabi-gcc -c $ASFLAGS startup.S -o startup.o
arm-none-eabi-gcc $CFLAGS main.c -o main.o -I./includes
arm-none-eabi-gcc $CFLAGS twice.c -o twice.o -I./includes

# Link everything together to the final executable
# -T specifies the linker script to use.
arm-none-eabi-gcc twice.o main.o startup.o $LDFLAGS -o capp.out
