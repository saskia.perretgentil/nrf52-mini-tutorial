#!/bin/sh

# Compile the two assembly files
# -c flag tells gcc to only compile, without linking
# -g flag tells gcc to insert debug information into object file
arm-none-eabi-as -g startup.S -o startup.o
arm-none-eabi-as -g main.S -o main.o

# Link everything together to the final executable
# -T specifies the linker script to use.
arm-none-eabi-gcc main.o startup.o -Wl,--gc-sections -mthumb -mabi=aapcs -lgcc -Xlinker -Map=startupApplication.map -Tnrf52.ld -lc --specs=nano.specs -lnosys -mcpu=cortex-m4 -o startupApplication.out
